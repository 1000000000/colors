{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Monad.Random

import Data.Function (on)
import Data.List (sortBy)
import Data.Ratio

import Graphics.UI.WX hiding (Event,Vector,rect,on)
import qualified Graphics.UI.WX as G

import Reactive.Banana
import Reactive.Banana.WX

data QColor = QColor
  { qred   :: Rational
  , qgreen :: Rational
  , qblue  :: Rational
  }

data DrawRect = DrawRect
  { rect    :: Rect
  , dColor  :: QColor
  , target  :: Color
  , created :: Integer
  }

type Draw = [DrawRect]

data DrawState g = DrawState
  { draw    :: Draw
  , randGen :: g
  , uColor  :: Color
  , uCenter :: Point
  , time    :: Integer
  }

stutter :: Int
stutter = 100

colorStep, colorVar :: Integer
colorStep = 20
colorVar  = 40

centerStepX, centerVarX, centerStepY, centerVarY :: Rect -> Int
centerStepX view = max 1 $ rectWidth  view `div` 1
centerVarX  view = max 1 $ rectWidth  view `div` 5
centerStepY view = max 1 $ rectHeight view `div` 1
centerVarY  view = max 1 $ rectHeight view `div` 5

numRects :: Int
numRects = 60

stepFreq :: Int
stepFreq = 60

qcolorRGB :: Integer -> Integer -> Integer -> QColor
qcolorRGB r g b = QColor (r % 256) (g % 256) (b % 256)

centerBounds :: Rect -> Rect
centerBounds view = rectBetween (point (rectLeft view + hMargin) (rectTop view + vMargin)) (point (rectRight view - hMargin) (rectBottom view - vMargin))
  where hMargin = min (rectWidth view) (rectHeight view) `div` 20
        vMargin = hMargin

qcolorToColor :: QColor -> Color
qcolorToColor (QColor qr qg qb) = rgb (floor $ 256*qr) (floor $ 256*qg) (floor $ 256*qb)

drawColorRect :: DC () -> DrawRect -> IO ()
drawColorRect dc (DrawRect rect dColor _ _) = drawRect dc rect [ pen := penTransparent, bgcolor := qcolorToColor dColor ]

drawCanvas :: Draw -> DC () -> Rect -> IO ()
drawCanvas draw dc _ = mapM_ (drawColorRect dc) draw

shrinkRect :: Rect -> Rect
shrinkRect r = rectCentralRect r $ (Size `on` max 0 . subtract 2 . ($r)) rectWidth rectHeight

nextColor :: DrawRect -> QColor
nextColor (DrawRect rect (QColor qr qg qb) target _) = QColor (qr - (qr - r) / steps) (qg - (qg - g) / steps) (qb - (qb - b) / steps)
  where r = (%256) . fromIntegral $ colorRed target
        g = (%256) . fromIntegral $ colorGreen target
        b = (%256) . fromIntegral $ colorBlue target
        steps = fromIntegral $ rectWidth rect `div` 2

nextRect :: MonadRandom m => Rect -> Color -> Point -> Integer -> DrawRect -> m DrawRect
nextRect view uColor uCenter time dRect
  | rectIsEmpty . rect $ shrunk = newRect view uColor uCenter time
  | otherwise                   = return shrunk
  where shrunk = dRect { rect = shrinkRect $ rect dRect, dColor = nextColor dRect }

reflect :: (Num a, Ord a, Show a) => a -> a -> a -> a
reflect lo hi x
  | lo > hi = error $ show lo ++ " > " ++ show hi
  | x >= lo && x <= hi = x
  | x < lo = reflect lo hi $ 2*lo - x
  | x > hi = reflect lo hi $ 2*hi - x

nextDraw :: RandomGen g => Rect -> DrawState g -> DrawState g
nextDraw view (DrawState draw gen0 uColor uCenter time) = DrawState (sortBy (compare `on` created) $ fmap (\x -> x { rect = rectMove (rect x) shift }) newDraw) newGen newColor newCenter (time+1)
  where (newDraw,   gen1) = runRand (mapM (nextRect view uColor uCenter time) draw) gen0
        (newColor,  gen2) = flip runRand gen1 $ do
                                r <- reflect 0 255 . (+colorRed   uColor) <$> getRandomR (-colorStep, colorStep)
                                g <- reflect 0 255 . (+colorGreen uColor) <$> getRandomR (-colorStep, colorStep)
                                b <- reflect 0 255 . (+colorBlue  uColor) <$> getRandomR (-colorStep, colorStep)
                                return $ rgb r g b
        (newCenter, gen3) = flip runRand gen2 $ do
                                cx <- reflect (rectLeft $ centerBounds view) (rectRight $ centerBounds view) . (+pointX uCenter) <$> getRandomR (negate $ centerStepX view, centerStepX view)
                                cy <- reflect (rectTop $ centerBounds view) (rectBottom $ centerBounds view) . (+pointY uCenter) <$> getRandomR (negate $ centerStepY view, centerStepY view)
                                return $ point cx cy
        (shift,   newGen) = runRand (liftA2 vector (getRandomR (-stutter, stutter)) (getRandomR (-stutter, stutter))) gen3

newRect :: MonadRandom m => Rect -> Color -> Point -> Integer -> m DrawRect
newRect view cColor cPoint time = do
  let shortDim = min (rectWidth view) (rectHeight view)
  centerX <- max (rectLeft $ centerBounds view) . min (rectRight $ centerBounds view) <$> getRandomR (pointX cPoint - centerVarX view, pointX cPoint + centerVarX view)
  centerY <- max (rectTop $ centerBounds view) . min (rectBottom $ centerBounds view) <$> getRandomR (pointY cPoint - centerVarY view, pointY cPoint + centerVarY view)
  rad <- getRandomR (max 1 $ shortDim `div` 20, minimum [centerX, centerY, rectWidth view - centerX, rectHeight view - centerY])
  r           <- max 0 . min 255 <$> getRandomR (colorRed   cColor - colorVar, colorRed   cColor + colorVar)
  g           <- max 0 . min 255 <$> getRandomR (colorGreen cColor - colorVar, colorGreen cColor + colorVar)
  b           <- max 0 . min 255 <$> getRandomR (colorBlue  cColor - colorVar, colorBlue  cColor + colorVar)
  (tr :: Int) <- getRandomR (0,255)
  (tg :: Int) <- getRandomR (0,255)
  (tb :: Int) <- getRandomR (0,255)
  return DrawRect
    { rect    = Rect (centerX + rectLeft view - rad) (centerY + rectTop view - rad) (2*rad) (2*rad)
    , dColor  = qcolorRGB r g b
    , target  = rgb tr tg tb
    , created = time
    }

networkDescription :: Panel () -> Timer -> MomentIO ()
networkDescription canvas timer = do
  canvasArea <- behavior canvas area
  curArea <- valueB canvasArea
  (ur :: Int) <- liftIO $ randomRIO (0,255)
  (ug :: Int) <- liftIO $ randomRIO (0,255)
  (ub :: Int) <- liftIO $ randomRIO (0,255)
  let uColor  = rgb ur ug ub
  uCenterX <- liftIO $ randomRIO (rectLeft $ centerBounds curArea, rectRight $ centerBounds curArea)
  uCenterY <- liftIO $ randomRIO (rectTop $ centerBounds curArea, rectBottom $ centerBounds curArea)
  let uCenter = point uCenterX uCenterY
  rects <- liftIO . replicateM numRects $ newRect curArea uColor uCenter 0
  gen <- liftIO getStdGen
  etick <- event0 timer command
  draws <- accumB (DrawState rects gen uColor uCenter 1) $ (pure nextDraw <*> canvasArea) <@ etick
  sink canvas [ G.on paint :== drawCanvas . draw <$> draws ]
  reactimate $ repaint canvas <$ etick

main :: IO ()
main = start $ do
  ff <- frame [ text := "Colors" ]
  tt <- timer ff [ interval := 1000 `div` stepFreq ]
  pp <- panel ff [ bgcolor := black ]
  set ff [ layout := expand $ widget pp ]
  network <- compile $ networkDescription pp tt
  actuate network
